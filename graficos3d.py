import numpy as np
import matplotlib.pyplot as plt
from scipy import stats
from mpl_toolkits.mplot3d import Axes3D

estaturas=[1.59,1.67,1.55,1.76,1.6,1.95,1.65,1.82,1.99,1.55,1.67,1.72,1.82,1.72,
    1.63,1.75,1.79,1.73,1.63,1.77,1.75,1.63,1.73,1.81,1.6,1.57,1.68,1.76,
    1.58,1.71,1.7,1.79,1.6,1.65,1.68,1.64,1.62,1.7,1.6,1.47,1.5,1.53,1.55,
    1.57,1.6,1.83,1.7,1.57,1.6,1.83,1.8,1.63,1.54,1.87,1.58,1.47,1.54,1.62,
    1.68,1.75,1.83,1.73,1.5,1.66,1.78,1.75,1.78,1.7,1.73,1.7,1.8,1.75]
pesos=[55,67,58,70,85,102,66,82,115,60,60,70,96,71,65,70,78,73,64,74,80,65,
    64,69,58,50,54,77,56,68,75,75,66,70,62,58,65,68.5,56.3,55,56,57,59,
    60,61,74,67,64,65,70,88,56,60,80,57,39.5,52,65,72,66,78,68,
    48,56,74,70,89,69,63,78,79,65]

# Se generan arreglos estilo numpy
estaturas = np.array(estaturas)
pesos = np.array(pesos)
imcs = pesos / estaturas ** 2

imcsNormalesIdx = (imcs >= 18.5) & (imcs <= 24.9)
imcsSuperioresIdx = (imcs >= 24.9) & (imcs <= 29.9)
imcsInferioresIdx = (imcs < 18.5)
imcsObesoIdx = (imcs > 29.9)

pesosObesidad = pesos[imcsObesoIdx]
estatObesidad = estaturas[imcsObesoIdx]
imcsObesidad = imcs[imcsObesoIdx]

pesosInferiores = pesos[imcsInferioresIdx]
estatInferiores = estaturas[imcsInferioresIdx]
imcsInferiores = imcs[imcsInferioresIdx]

pesosNormales = pesos[imcsNormalesIdx]
estatNormales = estaturas[imcsNormalesIdx]
imcsNormales = imcs[imcsNormalesIdx]

pesosSuperiores = pesos[imcsSuperioresIdx]
estatSuperiores = estaturas[imcsSuperioresIdx]
imcsSuperiores = imcs[imcsSuperioresIdx]


#Se genera una figura 3D
figura = plt.figure()
f3d = figura.add_subplot(1,1,1, projection='3d')
f3d.scatter(estatObesidad, pesosObesidad, imcsObesidad, c='red')
f3d.scatter(estatInferiores, pesosInferiores, imcsInferiores, c='blue')
f3d.scatter(estatNormales, pesosNormales, imcsNormales, c='green')
f3d.scatter(estatSuperiores, pesosSuperiores, imcsSuperiores, c='yellow')
