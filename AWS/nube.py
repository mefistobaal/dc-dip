import datetime
import numpy as np
import pymysql
import re, string, unicodedata
import nltk #lenguaje natural
import contractions #contracciones
import inflect #para entender verbos?
from wordcloud import WordCloud, STOPWORDS #hacer nube de palabras
import matplotlib as mpl 
from PIL import Image
mpl.use('Agg')
import matplotlib.pyplot as plt
from bs4 import BeautifulSoup #patrones
from nltk import word_tokenize, sent_tokenize #partir cadena dependiendo de un token
from nltk.corpus import stopwords #corpus:conjunto de terminus del context que estamos trabajando stopwords (palabras que no agregan valor)
from nltk.stem import LancasterStemmer, WordNetLemmatizer #las palabras muchas vienen de la msima flia (importancia – importante)
nltk.download('punkt') #puntuación
nltk.download('stopwords') #descarga elementos de las bibliotecas
nltk.download('wordnet')

#Nota: para instalar BeautifulSoup se debe instalar bs4, línea de comando sudo pip3 install bs4

HOST = "localhost"
USER = "root"
PASSWD = ""
DATABASE = "twitter"

db=pymysql.connect(host=HOST, user=USER, passwd=PASSWD, db=DATABASE, charset="utf8")
cursor = db.cursor()

#consulta a datos descargados anteriormente
query = ("SELECT screen_name, text from twitter")
cursor.execute(query)

texto=""
for (screen_name, text) in cursor:
  texto=texto+" "+text
cursor.close()
db.close()
# print(texto)

#se van agregar funciones
#1. quita todo el html

def strip_html(text):
    soup = BeautifulSoup(text, "html.parser")
    return soup.get_text()
    
#2. quita parentesis cuadrados

def remove_between_square_brackets(text):
    return re.sub('\[[^]]*\]', '', text)

#3. quita el ruido al texto (html quita parentesis cuadrados)

def denoise_text(text):
    text = strip_html(text)
    text = remove_between_square_brackets(text)
    return text

texto = denoise_text(texto)

#4. reemplaza contracciones

def replace_contractions(text):
    """Replace contractions in string of text"""
    return contractions.fix(text)

#tokenizar el texto (partir el texto por un toquen en este caso es por espacios)    
texto = replace_contractions(texto)
#print(texto)
palabras = nltk.word_tokenize(texto)
print(palabras[1:100])

#remueve datos
#1. remueve caracteres ascii
def remove_non_ascii(words):
    """Remove non-ASCII characters from list of tokenized words"""
    new_words = []
    for word in words:
        new_word = unicodedata.normalize('NFKD', word).encode('ascii', 'ignore').decode('utf-8', 'ignore')
        new_words.append(new_word)
    return new_words

# se convierte todo a minusculas
def to_lowercase(words):
    """Convert all characters to lowercase from list of tokenized words"""
    new_words = []
    for word in words:
        new_word = word.lower()
        new_words.append(new_word)
    return new_words

#si la palabra no esta dentro de las palabras del español(ididoma que busco) las sacas
def remove_punctuation(words):
    """Remove punctuation from list of tokenized words"""
    new_words = []
    for word in words:
        new_word = re.sub(r'[^\w\s]', '', word)
        if new_word != '':
            new_words.append(new_word)
    return new_words

#se reemplazan los numeros por palabras
def replace_numbers(words):
    """Replace all interger occurrences in list of tokenized words with textual representation"""
    p = inflect.engine()
    new_words = []
    for word in words:
        if word.isdigit():
            new_word = p.number_to_words(word)
            new_words.append(new_word)
        else:
            new_words.append(word)
    return new_words

#
def remove_stopwords(words):
    """Remove stop words from list of tokenized words"""
    new_words = []
    for word in words:
        if word not in stopwords.words('spanish') and word not in stopwords.words('english'):
            new_words.append(word)
    return new_words
    
def remove_html(words):
    """Remove stop words from list of tokenized words"""
    new_words = []
    url_words=['https','http']
    for word in words:
        if word not in url_words:
            new_words.append(word)
    return new_words

def stem_words(words):
    """Stem words in list of tokenized words"""
    stemmer = LancasterStemmer()
    stems = []
    for word in words:
        stem = stemmer.stem(word)
        stems.append(stem)
    return stems

def lemmatize_verbs(words):
    """Lemmatize verbs in list of tokenized words"""
    lemmatizer = WordNetLemmatizer()
    lemmas = []
    for word in words:
        lemma = lemmatizer.lemmatize(word, pos='v')
        lemmas.append(lemma)
    return lemmas

#normaliza
def normalize(words):
    words = remove_non_ascii(words)
    words = to_lowercase(words)
    words = remove_punctuation(words)
    words = replace_numbers(words)
    words = remove_stopwords(words)
    words = remove_html(words)
    return words

palabras = normalize(palabras)

def stem_and_lemmatize(words):
    stems = stem_words(words)
    lemmas = lemmatize_verbs(words)
    return stems, lemmas

stems, lemmas = stem_and_lemmatize(palabras)

texto=" ".join(palabras)
#print(palabras[1:100])
print(texto)

#grafica las palabras más usadas de la información descargada
maskArray = np.array(Image.open("cloud.jpg"))
cloud = WordCloud(background_color = "white", max_words = 200, mask = maskArray,width=1200,height=1000, stopwords = set(STOPWORDS))
cloud.generate(texto)
cloud.to_file("wordCloud.png")

