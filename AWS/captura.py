from __future__ import print_function
import tweepy
import json
import mysql.connector
from dateutil import parser
import sys
import importlib
importlib.reload(sys)


WORDS = ['']

CONSUMER_KEY = ""
CONSUMER_SECRET = ""
ACCESS_TOKEN = ""
ACCESS_TOKEN_SECRET = ""

HOST = "localhost"
USER = "root"
PASSWD = ""
DATABASE = "twitter"


def store_data(created_at, text, screen_name, tweet_id):
    db=mysql.connector.connect(host=HOST, user=USER, passwd=PASSWD, db=DATABASE, charset="utf8")
    cursor = db.cursor()
    insert_query = "INSERT INTO twitter (tweet_id, screen_name, created_at, text) VALUES (%s, %s, %s, %s)"
    cursor.execute(insert_query, (tweet_id, screen_name, created_at, text))
    db.commit()
    cursor.close()
    db.close()
    return

class StreamListener(tweepy.StreamListener):
    #acceso y manejadores del sttreamming api

    def on_connect(self):
        # se llama al conectar
        print("Nos conectamos")

    def on_error(self, status_code):
        # error
        print('error: ' + repr(status_code))
        return False

    def on_data(self, data):
        #cuando lleguen datos que pasa
        try:
           # Decode  JSON de Twitter
            datajson = json.loads(data)

            #extraer datos de json
            text = datajson['text']
            screen_name = datajson['user']['screen_name']
            tweet_id = datajson['id']
            created_at = datajson['created_at']

            #mostrar que llego en consola
            print("Tweet  ha llegado " + str(text))

            #insertar el tweet en base de datos
            store_data(created_at, text, screen_name, tweet_id)

        except Exception as e:
           print(e)

auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
#Set up the listener. The 'wait_on_rate_limit=True' is needed to help with Twitter API rate limiting.
listener = StreamListener(api=tweepy.API(wait_on_rate_limit=True))
streamer = tweepy.Stream(auth=auth, listener=listener)
print("Siguiendo palabras: " + str(WORDS))
streamer.filter(track=WORDS)
