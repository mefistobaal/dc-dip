import numpy as np
import matplotlib.pyplot as plt

# Array de datos
ests = [1.59, 1.67, 1.55, 1.76, 1.6, 1.95, 1.65, 1.82, 1.99, 1.55, 1.67, 1.72,
        1.82, 1.72, 1.63, 1.75, 1.79, 1.73, 1.63, 1.77, 1.75, 1.63, 1.73, 1.81,
        1.6, 1.57, 1.68, 1.76, 1.58, 1.71, 1.7, 1.79, 1.6, 1.65, 1.68, 1.64, 1.62,
        1.7, 1.6, 1.47, 1.5, 1.53, 1.55, 1.57, 1.6, 1.83, 1.7, 1.57, 1.83, 1.8, 1.63,
        1.54, 1.87, 1.58, 1.47, 1.54, 1.62, 1.68, 1.75, 1.83, 1.73, 1.5, 1.66, 1.78,
        1.75, 1.78, 1.7, 1.73, 1.7, 1.8, 1.75]
pesos = [55, 67, 58, 70, 85, 102, 66, 82, 115, 60, 60, 70, 96, 71, 65, 70, 78, 73,
         64, 74, 80, 65, 64, 69, 58, 50, 54, 77, 56, 68, 75, 75, 66, 70, 62, 58, 65,
         68.5, 56.3, 55, 56, 57, 59, 60, 61, 74, 67, 64, 65, 88, 56, 60, 80, 57,
         39.5, 52, 65, 72, 66, 78, 68, 48, 56, 74, 70, 89, 69, 63, 78, 79, 65]


# Funcion para calculo de imc
def imc(est, pes):
    return pes / (est ** 2)


'''
for i in range(len(ests)):
    print(imc(ests[i], pesos[i]))
'''

# Se crean arreglos de estilo NumPy
# ests = np.array(ests)
# pesos = np.array(pesos)

# Se ejecuta la formula de IMC directamente como una operacion
# NumPy la ejecuta como una funcion
# imcs = pesos / (ests**2)
# print(val)

# Se genera grafico Caja y vigotes
# plt.boxplot(imcs)
# plt.boxplot(ests)
# plt.boxplot(pesos)

# Grafico de dispersion
# plt.scatter(ests, pesos)
# Exporte de grafico
# plt.savefig('dispersion.png')
# plt.clf()

# Media, mediana y desviacion estandart
# print(np.mean(imcs))
# print(np.median(imcs))
# print(np.std(imcs))
# print(np.mean(imcs) - np.std(imcs))

# Datos aleatorios
np.random.seed(0)
estaturash = np.round(np.random.normal(1.72, 0.14, 100000), 2)
estaturasm = np.round(np.random.normal(1.59, 0.11, 100000), 2)
estats = np.concatenate([estaturash, estaturasm])
# print(estats)

pesosh = np.round(np.random.normal(67, 10, 100000), 2)
pesosm = np.round(np.random.normal(61, 5, 100000), 2)
pesoss = np.concatenate([pesosh, pesosm])
# print(pesoss)

imcs = pesoss / (estats ** 2)
# plt.hist(imcs, bins=500)
# plt.savefig('cajas.png')
# plt.clf()

# imcs = pesos / (ests**2)
# plt.hist(imcs, bins=50, cumulative=True)
imcsNormales = (imcs >= 18.5) & (imcs <= 24.9)
imcsSuperiores = (imcs >= 24.9) & (imcs <= 29.9)
imcsInferiores = (imcs < 18.5)
imcsObeso = (imcs > 29.9)

pesosObesidad = pesoss[imcsObeso]
estatObesidad = estats[imcsObeso]

pesosInferiores = pesoss[imcsInferiores]
estatInferiores = estats[imcsInferiores]

pesosNormales = pesoss[imcsNormales]
estatNormales = estats[imcsNormales]

pesosSuperiores = pesoss[imcsSuperiores]
estatSuperiores = estats[imcsSuperiores]

plt.scatter(estatObesidad, pesosObesidad, c='red')
plt.scatter(estatInferiores, pesosInferiores, c='yellow')
plt.scatter(estatNormales, pesosNormales, c='green')
plt.scatter(estatSuperiores, pesosSuperiores)

print(np.corrcoef(imcs, pesoss))
print(np.corrcoef(imcs, estats))

# plt.clf()
# print(imcsNormales, imcsSuperiores, imcsInferiores, imcsObeso)
